# SMARTPHONES
### Insegurs per 3 bandes

#VSLIDE

## Ofereix les funcionalitats i els riscos de:

* La telefonia mòbil
* Els dispositius _smart_
* Els ordinadors personals

#VSLIDE

## Judicis en joc

* Comoditat vs Llibertat
* Socialitzar vs Coherència
* Predicar vs Anonimat


#HSLIDE

# Telèfon mòbil

![telefon-mobil](img/telefon/mobil.png)

#VSLIDE

## Xarxa de telefonia

* Xarxa "ceŀlular": composta per ceŀles.
* Estacions base amb 1 o més antenes
* Cada antena genera una "ceŀla de cobertura"
* Estacions base interconnectades

#VSLIDE

![mapa-estacions-base](img/telefon/mapa-estacions-base.png)

_Estacions base de Movistar al centre de Barcelona_

#VSLIDE

![estacio-base](img/telefon/estacio-base.png)

_Estació base de tres ceŀles_

#VSLIDE

## Retenció de dades

_«Espionatge massiu retroactiu»_

Les operadores de **telefonia** i proveïdores d'**internet** han d'emmagatzemar durant **1 any** totes la **informació associada** a les comunicacions transportin.


#VSLIDE

## Metadades retingudes

* Identificador del mòbil (IMEI)
* Identificador de la tarjeta (IMSI)
* Remitent i destí de trucades, sms
* Connexions de dades: volum, duració
* Hora i lloc (gràcies a les ceŀles)

#VSLIDE

## Conseqüències

* Rutina diària: llocs on trobar-te
* Graf de relacions socials: família, amistats…
* Presència demostrable en un lloc i hora

#VSLIDE

## Cronologia

* 2006 [Directiva UE](https://quematumovil.pimienta.org/la_retencion_de_datos.html)
* 2007 [Llei a Espanya](https://www.internautas.org/html/5661.html)
* 2014-abril [Tribunal UE invalida directiva](https://edri.org/european-court-confirms-strict-safeguards-essential-data-retention/)
* 2015-desembre [Protesta de la CE](https://edri.org/council-data-retention-can-we-please-just-have-it-back/)
* 2016-juliol [Tribunal europeu reafirma](https://edri.org/european-court-confirms-strict-safeguards-essential-data-retention/)

#VSLIDE

## Casos de publicació voluntària

* Spitz i Ockenden van reclamar-ne una còpia
* Glättli va emmagatzemar-les per compte propi

_Austràlia va aprovar lleis molt similars a les europees_

#VSLIDE

![retencio-dades-spitz](img/telefon/retencio-dades-spitz.png)

2009: [Malte Spitz, eurodiputat verd](http://www.zeit.de/datenschutz/malte-spitz-data-retention)

#VSLIDE

![retencio-dades-glaettli](img/telefon/retencio-dades-glaettli.png)

2013: [Balthasar Glättli, diputat suís verd](https://www.digitale-gesellschaft.ch/dr.html)

#VSLIDE

![retencio-dades-ockenden](img/telefon/retencio-dades-ockenden.png)

2015: [Will Ockenden, periodista australià](http://www.abc.net.au/news/2015-08-16/metadata-retention-privacy-phone-will-ockenden/6694152)

#VSLIDE

## Intervenció de les comunicacions

1. Amb ordre judicial i conseqüent tuteŀla.
2. Sense ordre "en caso urgente" amb 72h.

#VSLIDE

## Tuteŀla judicial… efectiva?

* 17.000 condemnes en un any, trànsit a part ([font][1])
* ~100.000 intercepcions a l'any ([font][1])
* 1.675 jutjats dedicats a la investigació penal ([font][2])
* 40% jutjats: >150% "càrrega de treball raonable"
* 75% jutjats: >100% ([font][3])
* Confiança dels jutjats en els informes policials

[1]: http://www.eldiario.es/turing/vigilancia_y_privacidad/Vodafone-SITEL-espionaje_telefonico_0_269473073.html "eldiario.es: Cómo (y cuánto) puede espiar el gobierno tus llamadas"
[2]: http://www.eldiario.es/zonacritica/NSA-SITEL-caras-misma-moneda_6_190790927.html "eldiario.es: NSA y SITEL, dos caras de la misma moneda"
[3]: https://juecesparalademocracia.blogspot.com.es/2016/04/jpd-demanda-al-al-cgpj-y-al-ministerio.html "Jueces para la Democracia: JpD demanda al al CGPJ y al Ministerio de Justicia exigiendo limitación de cargas judiciales"

#VSLIDE

## Lleis mordassa: "Enjuiciamiento criminal"
![mordaza-nivelazo](img/telefon/mordaza-nivelazo.jpg)

#VSLIDE

_ARTÍCULO 588 TER D Solicitud de autorización judicial_

> [En caso de] elementos **terroristas** […] podrá ordenar [la interceptación] el **Ministro del Interior** […]. Esta medida se comunicará […] dentro del plazo máximo de **veinticuatro horas**[…]. El juez competente […] revocará o confirmará tal actuación en un plazo máximo de **setenta y dos horas** desde que fue ordenada la medida.

#VSLIDE

_ARTÍCULO 588 TER G Duración_

> La duración máxima inicial de la intervención, que se computará desde la fecha de autorización judicial, será de **tres meses**, **prorrogables** por períodos sucesivos de igual duración hasta el plazo máximo de **dieciocho meses**.

#VSLIDE

_ARTÍCULO 588 TER E Deber de colaboración_

> Todos los prestadores de servicios de telecomunicaciones […] están obligados a prestar al juez […] la asistencia y colaboración precisas para facilitar el cumplimiento de los autos de intervención de las telecomunicaciones.

…y a guardar secreto ([ref](colaboracion: http://legislacion.vlex.es/vid/ley-enjuiciamiento-criminal-real-septiembre-170233#section_60 "Ley de Enjuiciamiento Criminal, ARTÍCULO 588 TER E Deber de colaboración"))

#VSLIDE

## Recursos tècnics

* ***Lawful Interception*** La xarxa ceŀlular moderna porta intrínseca la capacitat d'intercepció en temps real de qualsevol comunicació no xifrada. ([ref][etsi-li], [ref][vodafone-li])
* **SITEL** fa ús d'aquesta i d'altres vulnerabilitats. La Policia Nacional, el CNI i la Guàrdia Civil hi tenen accés. ([ref][sitel])

[etsi-li]: http://www.etsi.org/technologies-clusters/technologies/lawful-interception "European Telecommunications Standards Institute: Lawful Interception"
[vodafone-li]: https://www.vodafone.com/content/sustainabilityreport/2014/index/operating_responsibly/privacy_and_security/law_enforcement.html#aaap "Vodafone, 2014: Law Enforcement Disclosure Report: 'Agency and authority powers: The legal context'"
[sitel]: https://ca.wikipedia.org/wiki/Sistema_Integrat_d'Interceptaci%C3%B3_de_Telecomunicacions "Viquipèdia: SITEL"

#VSLIDE

## Mòdem GSM aïllat
![arquitectura-modem-aillat](img/telefon/arquitectura-modem-aillat.png)

#VSLIDE

* Idealment, el hw el gestionaria un SO de codi lliure **(:**
* Però molts controladors/drivers no són lliures **\:**
* I… en realitat, el sistema operatiu tampoc ho és! **):**

#VSLIDE

## Mòdem GSM no aïllat
![arquitectura-modem-no-aillat](img/telefon/arquitectura-modem-no-aillat.png)

#VSLIDE

* …i a més, el xip de telefonia té accés directe a tots els altres components!! **D_;**



### L'**operadora** o **suplantadora** pot prendre el control del mòbil

#VSLIDE

## SITEL ho explota (?)

> cuando la llamada ha sido establecida y el móvil la recibe, antes de aceptarla el destinatario, el sistema comienza a grabar.

* [Notícia a eldiario.es](http://www.eldiario.es/politica/sentencia-convertir-telefonos-pinchados-microfonos_0_520398340.html "eldiario.es: Una sentencia revela que la policía puede convertir los teléfonos pinchados en micrófonos de ambiente")
* [Anàlisi legal per David Maeztu](http://www.derechoynormas.com/2016/05/tu-telefono-es-un-microfono-y-puede-ser.html "Tu teléfono es un micrófono y puede ser utilizado en tu contra (STS de 3 de mayo de 2016)")

#VSLIDE

## IMSI-Catchers

* Estació base de telefonia, **maliciosa** i desplegable d'**amagat** a l'operadora i usuàries.
* Permet **llistar** tots els mòbils en una zona propera: **dispositiu** (IMEI) i **usuari** (IMSI) ([ref][schneier-stingray])
* Permet **interceptar** converses, sms i dades fetes amb SIM anteriors al 2009 ([ref][gsmarena])
* Amb altres SIMs, pot aconseguir-se obligant a usar **estàndards antics** ([ref][banescu-posea])

[schneier-stingray]: https://www.schneier.com/blog/archives/2015/04/the_further_dem_1.html "Schneier on Security, 2015: The Further Democratization of Stingray"
[gsmarena]: http://www.gsmarena.com/the_gsm_encryption_algorithm_was_broken-news-1347.php "GSM Arena, 2009: The GSM encryption algorithm was broken"
[banescu-posea]: https://www.academia.edu/3224328/Security_of_3G_and_LTE "Banescu & Posea, 2013: Security of 3G and LTE"

#VSLIDE

![downgrade-attack](img/telefon/downgrade-attack.png)

_Atac per versió antiga: de LTE a 2G_ ([ref](https://theintercept.com/2016/09/12/long-secret-stingray-manuals-detail-how-police-can-spy-on-phones/ "The Intercept, 2016: Long-Secret Stingray Manuals Detail How Police Can Spy on Phones"))
#VSLIDE

## IMSI-Catcher per 1800$!

![imsi-catcher](img/telefon/imsi-catcher.jpg)

#VSLIDE

> […] you can catch all active UMTS mobile phones in your **proximity**. All captured data, such as **IMSI**, **IMEI**, **TMSI** will be stored […]. With our 3G UMTS IMSI Catcher you can **redirect** single UMTS mobile phones to specific GSM frequencies, in order to **monitor the conversation** with our active or passive cellular monitoring systems. 

Pot ser teu [ara mateix](https://www.alibaba.com/product-detail/IMSI-catcher_135958750.html "Alibaba.com: IMSI-catcher")

#VSLIDE

## Vulnerabilitat en SS7
* SS7 és un conjunt de protocols per **interconnectar** operadores → permet el **roaming**
* Quan es va dissenyar, hi havia *poques operadores*, molt grans i es coneixien → no semblava necessària l'**autenticació** entre elles
* Però ara **"qualsevol"** pot fer una operadora i el sistema no ha canviat → **frau**

[ref](http://berlin.ccc.de/~tobias/25c3-locating-mobile-phones.pdf "[pdf] Tobias Engels, 2014: Locating Mobile Phones using Signalling System #7")

#VSLIDE

## Enviant un SMS
![tobias-ss7](img/telefon/tobias-ss7.png)

#VSLIDE

## Conseqüències

* Es pot **suplantar** una operadora i per tant…
* Accedir a informació que permet la **localització**
* Demanar **claus de xifrat** de les comunicacions d'un usuari
* Redirigir una trucada cap a tu, **enregistrar-la** i enviar-la al destí legítim
* Enviar **SMS massivament**, en nom d'una altra operadora

#VSLIDE

## De debò?

* Ha passat cert temps des que va esclatar: desembre de 2014
* Les operadores intenten filtrar les comuniacions externes
* Servei comercial de localització al 2013: [SkyLock de Verint](https://apps.washingtonpost.com/g/page/business/skylock-product-description-2013/1276/ "Washington Post, 2014: For sale: Systems that can secretly track where cellphone users go around the globe")
* Com de difícil és aconseguir una connexió a SS7?

#VSLIDE

![skylock](img/telefon/skylock.png)

_Localització a partir del número de telèfon amb Skylock_

#VSLIDE

## Idees contra el rastreig a posteriori
* Deixar el mòbil encès a casa
* O bé apagar-lo abans de sortir
* Substituir trucades i sms per apps "segures"

#VSLIDE

## Idees contra micròfons secrets
* En comptes d'apagar, insonoritzar
* Comprovar si la bateria baixa tot i apagat
* Treure la bateria per canviar de segle

#VSLIDE

## Idees contra el rastreig indiscriminat

### ?

#VSLIDE

![quema-tu-movil](img/telefon/quema-tu-movil.png)

#HSLIDE

# Dispositiu smart

![smart](img/smart/cctv.gif)

#VSLIDE

## El micròfon, el sensor més antic
* I el més íntim.
* Les trucades permeten bona part de la comunicació cara-cara
* El que diem és mirall del que pensem i sentim
* Micròfon ambiental sempre a sobre.

#VSLIDE

## GPS

* Funciona amb satèŀlits "públics" dels USA
* Va a part de la xarxa telefònica (veu, dades i sms)
* Va a part de la Wi-Fi, Bluetooth, NFC…
* No envia cap senyal, només «escolta»


#VSLIDE

## Sensors de moviment i orientació

* Giroscopi: angles d'inclinació respecte el terra
* Acceleròmetre: canvi de velocitat i gravetat. Sacseig
* Brúixola: orientació respecte el nord magnètic
* Baròmetre: com a altímetre, ajuda el GPS

#VSLIDE

## Localització assistida (aGPS)

S'agilitza el GPS amb una aproximació a partir de

* Ceŀles telefòniques properes
* Les Wi-Fi properes (MAC dels AP)

#VSLIDE

## Perill

* Bases de dades mundials privades de Google
* Registre de Google de la teva posició

#VSLIDE

## Historial d'ubicacions de Google

> prediccions per als desplaçaments diaris, resultats de la cerca millorats i anuncis més útils tant a Google com en altres llocs.

#VSLIDE

> les dades d'ubicació es poden desar des de qualsevol dels dispositius en què hagis iniciat la sessió i per als quals hagis activat l'Historial d'Ubicacions.

Consulta el teu [historial d'ubicacions](https://www.google.com/maps/timeline "Google Maps: Cronologia")

#VSLIDE

![google-maps-cronologia](img/smart/google-maps-cronologia.jpg)

_L'aplicació de «Cronologia» iŀlustra el rastreig de Google_

#VSLIDE

## Desactiva la recoŀlecció d'ubicacions

* Respon sempre NO si et demanen permís per la ubicació
* [Android genèric](https://support.google.com/accounts/answer/3118687 "Activar o desactivar l'Historial d'Ubicacions")
* [Android 4.1-4.3](https://support.google.com/nexus/answer/2819558 "Accedir a la ubicació amb versions d'Android entre la 4.1 i la 4.3")

#VSLIDE

## El problema amb la ubicació

* Rutina → activitat predictible
* Graf social
* Activitats especials → més predisposició a comprar?
* Activitat política (locals, barris)
* Informació pròpia, de gent propera, i de grups socials
* **Publicitat invasiva**: dinamita la **llibertat de pensament**
* **Dictadura digital**: la informació és poder

#VSLIDE

## Càmeres i seguretat de la informació

* Frontal: tapem-la com les webcams dels portàtils
* Posterior: fotos/vídeos indiscrets en tot moment

#VSLIDE

## Càmeres i seguretat psico-social

* Les imatges són motors d'«espirals d'enveja» ([ref][slate])
* Penjar _selfies_ empitjora l'autoestima de la resta ([ref][psych-central])
* L'ús de les xarxes socials provoca o genera persones deprimides ([ref][science-daily])

[slate]: http://www.slate.com/articles/technology/technology/2013/07/instagram_and_self_esteem_why_the_photo_sharing_network_is_even_more_depressing.html "Slate, 2013: 'Selfie-Loathing: Instagram is even more depressing than Facebook. Here’s why.'"
[psych-central]: https://psychcentral.com/news/2016/10/21/obsessive-viewing-of-online-selfies-can-damage-self-esteem/111435.html "Psych Central, 2016: Obsessive Viewing of Online Selfies Can Damage Self-Esteem"
[science-daily]: https://www.sciencedaily.com/releases/2016/03/160322100401.htm "Science Daily, 2016: Social media use associated with depression among US young adults"

#VSLIDE

## Autoestima a facebook

![what-on-mind](img/smart/what-on-mind.jpg)

["What's on your mind"](https://vimeo.com/97115097 "Shaun Higton: What's on your Mind")

#VSLIDE

## Sensors ambientals
* Lluminositat: lluïssor de la pantalla
* Proximitat: detecta una orella propera en les trucades
* Temperatura: meteorologia
* Humitat: meteorologia
* Baròmetre: meteorologia

#VSLIDE

## Detectors de tapa tancada

* Interruptor _reed_
* D'efecte Hall

#VSLIDE

## Pantalla tàctil

* Sensors "capacitius"
* Perills
    * Empremta personal, gestos dels dits únics
    * Emprempta dactilar amb el greix natural
    * El greix pot revelar el patró o contrasenya

#VSLIDE

## _Wearables_

* Ritme cardíac
* Passes
* Distància
* Qualitat del son

#VSLIDE
![smartwatch](img/smart/smartwatch.jpg)

#VSLIDE

## _Quantified Self_

* [Moviment tenir una vida registrada en números](http://quantifiedself.com/ "Quantified Self: self knowdlege through numbers")
* [Dades menstruals i quantificació masclista](https://chupadados.codingrights.org/es/menstruapps-como-transformar-sua-menstruacao-em-dinheiro-para-os-outros/ "Chupadatos: MENSTRUAPPS – ¿Cómo convertir tu menstruación en dinero (para los demás)?")

#VSLIDE

## Reflexió

#### Tenim por que ens implantessin un xip sota la pell i el portem a la butxaca, al canell i a les orelles.

#VSLIDE

## Idees

* Tapar la càmera frontal
* No participar en la moda dels _smartwatch_
* Denegar a les app tots els permisos per defecte
* Denegar sempre la petició d'ubicació
* Fer servir mapes offline com [OsmAnd](https://f-droid.org/repository/browse/?fdfilter=osmand&fdid=net.osmand.plus) i [Maps.me](https://maps.me/en/download)
* Snowden: Arrencar el micròfon i només quan calgui connectar uns auriculars amb micròfon
* Fer servir un mòbil no-smart

#HSLIDE

# Ordinador personal

![portatil](img/ordinador/portatil.png)

#VSLIDE

## Primera pregunta

### Confiem en la persona amb qui ens comuniquem?

#VSLIDE

## On the internet…

![on-the-internet](img/ordinador/on-the-internet.jpg)

#VSLIDE

## Segona pregunta

### El contingut que comparteixo m'identifica?

#VSLIDE

## Perills
* Metadades incrustades (pdf, fotos, vídeos, etc)
* La xarxa de seguidores m'identifica
* El patró horari em delata
* Repeteixo expressions o faltes d'ortografia concretes

#VSLIDE

## Perills al dispositiu

* Físics: robatori, malfunció
* Sistema operatiu
* Aplicacions

#VSLIDE

## Perills físics

* Robatori: accés al mail i a totes les aplicacions
* Sabotatge: no disponibilitat i pèrdua de dades (?)
* No reparable, obsolescència

#VSLIDE

## Sistema operatiu

* Bloatware espia, privatiu, pesat
* Backdoors: PlayStore n'és una
* Codi privatiu: teclat, cerques
* Actualitzacions de seguretat

#VSLIDE

## Aplicacions

* Jocs que demanen molts permisos
* Aplicació espia per algú proper
* Aplicació legítima enverinada
* Errors no arreglats

#VSLIDE

## Nivells de la xarxa
* Xarxa local
* Xarxa d'accés + troncal
* Servidors finals

#VSLIDE

## Agents possibles
* Un particular malintencionat
* Investigador privat
* Policia amb o sense ordre jodicial
* Màfies amb recursos
* Traficants de dades personals autònoms
* Empreses que fan negoci amb dades
* Agències d'espionatge

#VSLIDE

## Idees
* Comunicar com de secreta és una informació
* Xifrar els discos durs
* Contrasenyes fortes i úniques
* Usar VPN o Tor
* Wi-Fi: no WPS, WPA2, contrasenya canviada
* Sistema operatiu lliure
* Evitar serveis privatius i centralitzats

#HSLIDE

### Gràcies per la vostra atenció. Estem en contacte!
* https://criptica.org
* https://twitter.com/CripticaOrg
* https://quitter.cat/criptica
* Apunta't a la [llista de correu](https://www.autistici.org/mailman/listinfo/list_criptica)!

